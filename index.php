<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="./assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="./lib/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
	<link rel="stylesheet" type="text/css" href="./assets/css/popup.css">
</head>
<body>
	<header>
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light ">
				<a class="navbar-brand" href="#">
					<img src="./assets/images/logo-site.png">
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
						<li class="nav-item active">
							<a class="nav-link" href="#">TRANG CHỦ</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#gioi-thieu">GIỚI THIỆU</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#thanh-phan">THÀNH PHẦN</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#tin-tuc">TIN TỨC</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#phan-hoi">PHẢN HỒI</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#ve-chung-toi">VỀ CHÚNG TÔI</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#lien-he">LIÊN HỆ</a>
						</li>
						<li class="nav-item search_box"></li>
						<li class="nav-item national">
							<a href="#lien-he" class="btn btn-buynow">
								MUA NGAY
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</header>
	<div class="wrap-content">
		<div class="section section-header_slider">
			<div class="header_slider">
				<div class="slider-item" style="background-image: url(./assets/images/img-header.jpg)">
					<div class="left"></div>
					<div class="right">
						<div class="wrap-desc wow animated bounceInUp slow " data-wow-duration="1s" data-wow-delay="3s" data-wow-iteration="5">
							<h2>100% TỪ THIÊN NHIÊN <br> 185.000đ/1 hộp</h2>
							<p>Nguyên lý cộng hưởng trong quá trình chiết suất phân đoạn các dược liệu thành phần của 7 Plus. Đặc biệt việc phối kết hợp 2 thành phần Diếp Cá, Diếp Trời có tác dụng tuyệt vời cho việc co hồi búi trĩ.</p>
							<a href="#lien-he" class="btn btn-custom">MUA NGAY</a>
						</div>
					</div>
				</div>
				<div class="slider-item" style="background-image: url(./assets/images/img-header.jpg)">
					<div class="left"></div>
					<div class="right">
						<div class="wrap-desc" data-wow-duration="1s" data-wow-delay="3s" data-wow-iteration="5">
							<h2>100% TỪ THIÊN NHIÊN <br> 185.000đ/1 hộp</h2>
							<p>Nguyên lý cộng hưởng trong quá trình chiết suất phân đoạn các dược liệu thành phần của 7 Plus. Đặc biệt việc phối kết hợp 2 thành phần Diếp Cá, Diếp Trời có tác dụng tuyệt vời cho việc co hồi búi trĩ.</p>
							<a href="#lien-he" class="btn btn-custom">MUA NGAY</a>
						</div>
					</div>
				</div>
				<div class="slider-item" style="background-image: url(./assets/images/img-header.jpg)">
					<div class="left"></div>
					<div class="right">
						<div class="wrap-desc" data-wow-duration="2s" data-wow-delay="5s" data-wow-iteration="5">
							<h2>100% TỪ THIÊN NHIÊN <br> 185.000đ/1 hộp</h2>
							<p>Nguyên lý cộng hưởng trong quá trình chiết suất phân đoạn các dược liệu thành phần của 7 Plus. Đặc biệt việc phối kết hợp 2 thành phần Diếp Cá, Diếp Trời có tác dụng tuyệt vời cho việc co hồi búi trĩ.</p>
							<a href="#lien-he" class="btn btn-custom">MUA NGAY</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="gioi-thieu" class="section section-intro" >
			<div class="wrap-intro container">
				<div class="left">
					<img src="./assets/images/img-branch.png">
				</div>
				<div class="right">
					<div class="wrap-right">
						<h2>GIỚI THIỆU</h2>
						<p class="text_100_percent"><img src="./assets/images/text_100_percent.png" class="img-responsive"></p>
						<p><img src="./assets/images/icon-star.png" class="img-responsive"></p>
						<p>7Plus được nghiên cứu bào chế theo nguyên lý cộng hưởng chiết xuất phân đoạn từ 100% thảo dược từ thiên nhiên, đặc biệt là Diếp Cá và Diếp Trời. Sản phẩm mang lại những tính năng tuyệt vời trong hỗ trợ điều trị trĩ.</p>
						<ul>
							<li>Làm co búi trĩ.</li>
							<li>Tăng cường nhu động ruột, giúp nhuận tràng, chống táo bón.</li>
							<li>Cải thiện các triệu chứng khó chịu của bệnh trĩ cấp tính và mãn tính.</li>
						</ul>
					</div>
				</div>
			</div>
			<p class="big__text">XÓA TAN NỖI LO VỀ TRĨ</p>
		</div>
		<div id="thanh-phan" class="section section-components">
			<h2>THÀNH PHẦN</h2>
			<div class="container">
				<ul>
					<li>
						<div class="img">
							<img src="./assets/images/img-1.png">
						</div>
						<div class="desc">
							<div>
								<h4>HOÀNG KỲ</h4>
								<p>Giúp ổn định tổn thương niêm mạc nơi hậu môn và trực tràng.</p>
							</div>
						</div>
					</li>
					<li>
						<div class="img">
							<img src="./assets/images/img-2.png">
						</div>
						<div class="desc">
							<div>
								<h4>HOA HÒE</h4>
								<p>Giúp bền thành mạch và làm cho ổn định sự liên kết của đàm tĩnh mạch và động mạch nơi hậu môn và trực tràng</p>
							</div>
						</div>
					</li>
					<li>
						<div class="img">
							<img src="./assets/images/img-3.png">
						</div>
						<div class="desc">
							<div>
								<h4>BẠCH TRUẬT</h4>
								<p>Dự phòng nguy cơ táo bón và ổn định thu động đại tràng</p>
							</div>
						</div>
					</li>
					<li>
						<div class="img">
							<img src="./assets/images/img-4.png">
						</div>
						<div class="desc">
							<div>
								<h4>ĐẲNG SÂM</h4>
								<p>Đặc biệt tăng cường hệ tuần hoàn bạch huyết tập trung nơi hậu môn và trực tràng</p>
							</div>
						</div>
					</li>
					<li>
						<div class="img">
							<img src="./assets/images/img-5.png">
						</div>
						<div class="desc">
							<div>
								<h4>DIẾP TRỜI</h4>
								<p>Dự phòng nguy cơ táo bón và ổn định thu động đại tràng</p>
							</div>
						</div>
					</li>
					<li>
						<div class="img">
							<img src="./assets/images/img-6.png">
						</div>
						<div class="desc">
							<div>
								<h4>DIẾP CÁ</h4>
								<p>Đặc biệt tăng cường hệ tuần hoàn bạch huyết tập trung nơi hậu môn và trực tràng</p>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div id="phan-hoi"  class="section section-about_us">
			<h2>BÁO CHÍ NÓI GÌ VỀ CHÚNG TÔI?</h2>
			<div class="container">
				<div class="slider_about_us">
					<div class="about_us_item">
						<div 
							class="img js-modal-btn" 
							data-video-id="8GAvVuRrNVA" 
							style="background-image: url(./assets/images/about-1.jpg);" 
						>
							<img src="./assets/images/icon-play.png">
						</div>
						<div 
							class="img js-modal-btn" 
							data-video-id="8GAvVuRrNVA" 
							style="background-image: url(./assets/images/about-2.jpg);">
							<img src="./assets/images/icon-play.png">
						</div>
					</div>
					<div class="about_us_item">
						<div 
							class="img js-modal-btn" 
							data-video-id="8GAvVuRrNVA" 
							style="background-image: url(./assets/images/about-2.jpg);">
							<img src="./assets/images/icon-play.png">

						</div>
						<div 
							class="img js-modal-btn"
							data-video-id="8GAvVuRrNVA"  
							style="background-image: url(./assets/images/about-2.jpg);">
							<img src="./assets/images/icon-play.png">

						</div>
					</div>
					<div class="about_us_item">
						<div 
							class="img js-modal-btn"
							data-video-id="8GAvVuRrNVA"  
							style="background-image: url(./assets/images/about-2.jpg);">
							<img src="./assets/images/icon-play.png">

						</div>
						<div 
							class="img js-modal-btn"
							data-video-id="8GAvVuRrNVA"  
							style="background-image: url(./assets/images/about-2.jpg);">
							<img src="./assets/images/icon-play.png">

						</div>
					</div>
					<div class="about_us_item">
						<div 
							class="img js-modal-btn"
							data-video-id="8GAvVuRrNVA"  
							style="background-image: url(./assets/images/about-2.jpg);">
							<img src="./assets/images/icon-play.png">

						</div>
						<div 
							class="img js-modal-btn"
							data-video-id="8GAvVuRrNVA"  
							style="background-image: url(./assets/images/about-1.jpg);">
							<img src="./assets/images/icon-play.png">

						</div>
					</div>
					<div class="about_us_item">
						<div 
							class="img js-modal-btn"
							data-video-id="8GAvVuRrNVA"  
							style="background-image: url(./assets/images/about-2.jpg);">
							<img src="./assets/images/icon-play.png">
						</div>
						<div 
							class="img js-modal-btn"
							data-video-id="8GAvVuRrNVA"  
							style="background-image: url(./assets/images/about-1.jpg);">
							<img src="./assets/images/icon-play.png">

						</div>
					</div>
					<div class="about_us_item">
						<div 
							class="img js-modal-btn"
							data-video-id="8GAvVuRrNVA"  
							style="background-image: url(./assets/images/about-2.jpg);">
							<img src="./assets/images/icon-play.png">

						</div>
						<div 
							class="img js-modal-btn"
							data-video-id="8GAvVuRrNVA"  
							style="background-image: url(./assets/images/about-1.jpg);">
							<img src="./assets/images/icon-play.png">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="section section-client">
			<div class="container">
				<div class="slider_client">
					<div class="client_item">
						<img src="./assets/images/icon_vtv2.jpg">
					</div>
					<div class="client_item">
						<img src="./assets/images/icon_ndds.jpg">
					</div>
					<div class="client_item">
						<img src="./assets/images/icon_bm.jpg">
					</div>
					<div class="client_item">
						<img src="./assets/images/icon_24h.jpg">
					</div>
					<div class="client_item">
						<img src="./assets/images/icon_skds.jpg">
					</div>
				</div>
			</div>
		</div>
		<div id="tin-tuc"  class="section section-blog">
			<h2>Cẩm nang thoát Trĩ</h2>
			<div class="container">
				<div class="blog">
					<div class="blog-item">
						<div class="img">
							<img src="./assets/images/blog-1.jpg">
						</div>
						<div class="desc">
							<h3><a href="">Ai bị trĩ, nứt kẽ hậu môn: Thử mẹo này xong hết đau, lại vui tươi phơi phới</a></h3>
							<a href="#">Xem chi tiết</a>
						</div>
					</div>
					<div class="blog-item">
						<div class="img">
							<img src="./assets/images/blog-2.jpg">
						</div>
						<div class="desc">
							<h3><a href="">Ai bị trĩ, nứt kẽ hậu môn: Thử mẹo này xong hết đau, lại vui tươi phơi phới</a></h3>
							<a href="#">Xem chi tiết</a>
						</div>
					</div>
					<div class="blog-item">
						<div class="img">
							<img src="./assets/images/blog-1.jpg">
						</div>
						<div class="desc">
							<h3><a href="">Ai bị trĩ, nứt kẽ hậu môn: Thử mẹo này xong hết đau, lại vui tươi phơi phới</a></h3>
							<a href="#">Xem chi tiết</a>
						</div>
					</div>
					<div class="blog-item">
						<div class="img">
							<img src="./assets/images/blog-2.jpg">
						</div>
						<div class="desc">
							<h3><a href="">Ai bị trĩ, nứt kẽ hậu môn: Thử mẹo này xong hết đau, lại vui tươi phơi phới</a></h3>
							<a href="#">Xem chi tiết</a>
						</div>
					</div>

					<div class="blog-item">
						<div class="img">
							<img src="./assets/images/blog-1.jpg">
						</div>
						<div class="desc">
							<h3><a href="">Ai bị trĩ, nứt kẽ hậu môn: Thử mẹo này xong hết đau, lại vui tươi phơi phới</a></h3>
							<a href="#">Xem chi tiết</a>
						</div>
					</div>
					<div class="blog-item">
						<div class="img">
							<img src="./assets/images/blog-1.jpg">
						</div>
						<div class="desc">
							<h3><a href="">Ai bị trĩ, nứt kẽ hậu môn: Thử mẹo này xong hết đau, lại vui tươi phơi phới</a></h3>
							<a href="#">Xem chi tiết</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="section section-status">
			<h2>Chuẩn đoán tình trạng</h2>
			<div class="container">
				<div class="wrap-status">
				<div class="wrap-type">
					<h3>PHÂN LOẠI</h3>
					<ul>
						<li><a href="">Trĩ nội (Internal hemorrhoids):</a> là bệnh mà các búi trĩ xuất phát từ các đám rối mạch máu tĩnh mạch ở bên trong hậu môn phía trên đường lược.</li>
						<li><a href="">Trĩ ngoại (External hemorrhoids):</a> là bệnh trĩ mà các búi trĩ xuất phát từ bên dưới đường lược và thường được che phủ bởi niêm mạc hoặc da ở rìa hậu môn.</li>
						<li><a href="">Trĩ hỗn hợp (Mixed hemorrhoids:</a> là do có sự kết hợp giữa các búi trĩ nội lẫn các búi trĩ ngoại.</li>
						<li><a href="">Trĩ vòng:</a> khi có nhiều hơn ba búi trĩ và chiếm gần hết toàn bộ vòng hậu môn thì được gọi là trĩ vòng.</li>
						<li><a href="">Trĩ thuyên tắc:</a> các mạch máu nơi có búi trĩ bị tắc nghẽn hay vỡ tạo thành các cục máu đông, gây đau đớn nhiều.</li>
					</ul>
				</div>
				<div class="wrap-level">
					<h3>CẤP ĐỘ</h3>
					<p>Đôi với trĩ nội, tùy theo mức đọ sa của búi trĩ mà ta có thể phân chia làm 4 cấp độ:</p>
					<ul>
						<li>Búi trĩ chỉ phình lên, không sa ra ngoài. Có thể chảy máu khi đi tiêu</li>
						<li>Búi trĩ  sa ra ngoài khi đi tiêu và ngay sau đó tự tụt vào.</li>
						<li>Búi trĩ  sa ra ngoài khi đi tiêu và khó tụt vào. Thường phải dùng tay đẩy vào.</li>
						<li>Búi trĩ  sa ra ngoài thường trực và khi lấy tay đẩy vào búi trĩ lại tụt ra.</li>
					</ul>
				</div>
				</div>
			</div>
		</div>
		<div class="section section-level">
			<img src="./assets/images/img-level.jpg">
		</div>
		<div id="ve-chung-toi" class="section section-about">
			<img src="./assets/images/bg_about_us_03.jpg">
		</div>
		<div id="lien-he" class="section section-contact">
			<div class="container">
				<div class="wrap-contact">
					<div class="left">
						<p>Đừng ngần ngại! Hãy gọi ngay đến tổng đài tư vấn miễn cước của chúng tôi để được tư vấn trực tiếp:</p>
						<span class="img_line_green"><img src="./assets/images/line-green.jpg"></span>
						<div class="group-btn">
							<p> <a href="tel:0967367037" class="btn btn-hotline">Hotline: <strong>0967 367 037</strong></a></p>
						</div>
						<img src="./assets/images/image_bag.jpg">
					</div>
					<div class="right">
						<div class="form-submit">
							<div class="desc">
								<strong>Đặt hàng ngay</strong>
								<p class="sub_text">NHẬN ƯU ĐÃI</p>
								<ul>
									<li>Miễn phí ship hàng toàn quốc.</li>
									<li>Tặng 01 sổ tay hiện đại tư vấn sức khỏe</li>
								</ul>
							</div>
							<form method="POST" action="./sendmail.php">
								<input type="text" required name="full_name" class="form-control" placeholder="Họ và tên...">
								<input type="text" required name="address" class="form-control" placeholder="Địa chỉ nhận hàng...">
								<input type="text" required name="phone" class="form-control" placeholder="Số điện thoại...">
								<input type="text" required name="qty" class="form-control" placeholder="Số lượng">
								<div class="text-center">
									<button class="btn btn-submit">MUA NGAY</button>
								</div>
								<?php
									if(isset($_GET['sendmail']) &&  $_GET['sendmail'] == true) {
										echo '<p class="status_sendmail text-center">Đặt hàng thành công.</p>';
									}
								?>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="section-googlemaps">
			<div id="map">
				<img src="./assets/images/img-map.jpg">
			</div>
		</div>
	</div>
	<footer>
		<div class="container">
			<div class="footer_top">
				<div class="column">
					<img src="./assets/images/logo-site.png">
					<p class="site-title">Trĩ lâu năm</p>
					<ul>
						<li><i class="fa fa-phone"></i> <a href="tel:0967367037">0967 367 037</a></li>
						<li><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:contact@7plus.com">contact@7plus.com</a></li>
						<li><i class="fa fa-map-marker" aria-hidden="true"></i> 549 Điện Biên Phủ, P3,Q3 TP.Hồ Chí Minh</li>
					</ul>
				</div>
				<div class="column">
					<h3>Liên kết</h3>
					<ul>
						<li><a href="#">Trang Chủ</a></li>
						<li><a href="#phan-hoi">Phản hồi</a></li>
						<li><a href="#gioi-thieu">Giới thiệu</a></li>
						<li><a href="#ve-chung-toi">Về chúng tôi</a></li>
						<li><a href="#thanh-phan">Thành phần</a></li>
						<li><a href="#lien-he">Liên hệ</a></li>
						<li><a href="#tin-tuc">Cẩm nang</a></li>
					</ul>
				</div>
				<div class="column">
					<h3>Hệ thống phân phối</h3>
					<ul>
						<li>Hà Nội</li>
						<li>Miền Trung</li>
						<li>Miền Nam</li>
					</ul>
				</div>
				<div class="column">
					<h3>Liên hệ với chúng tôi qua:</h3>
					<div class="social-icon">
						<a href="#"><img src="./assets/images/icon-fb.png"></a>
						<a href="#"><img src="./assets/images/icon-zalo.png"></a>
						<a href="#"><img src="./assets/images/icon-phone.png"></a>
						<a href="#"><img src="./assets/images/icon-phone-call.png"></a>
					</div>
					<div class="group-btn">
						<p> <a href="tel:0967367037" class="btn btn-hotline">Hotline: <strong>0967 367 037</strong></a></p>
					</div>
				</div>
			</div>
			<div class="footer_newsletter">
				<p>Đăng ký để nhận thông tin về sản phẩm</p>
				<div class="input-group mb-2">
					<input type="text" class="form-control" placeholder="Email Address" id="inlineFormInputGroup" placeholder="Username">
			        <div class="input-group-prepend">
			          	<button>Subscribe</button>
			        </div>
		      	</div>
			</div>
		</div>
		<div class="footer_copyright">
			<div class="container">
				<span>Copyright ©2019 7plus</span>
			</div>
		</div>
	</footer>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script type="text/javascript" src="./lib/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
	<script type="text/javascript" src="./assets/js/app.js"></script>
	<script type="text/javascript" src="./assets/js/modal.js"></script>

</body>
</html>