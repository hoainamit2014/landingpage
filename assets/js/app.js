$(document).ready(function() {
	// Auto scroll when have id
	$(window).on('load',function(){
		var hash = window.location.hash;
		if(hash){
			var pos = $(hash).offset().top - 75;
		    // animated top scrolling
		    $('body, html').animate({scrollTop: pos});
		}
	});
	
	$(window).on('scroll',function(){
		var header = $('header').height();
		var scrollTop = $(window).scrollTop();
		if(scrollTop >= header){
			$('header').addClass('fixed');
		}else{
			$('header').removeClass('fixed');
		}
	});

	$('.header_slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		fade:true,
		autoplaySpeed: 5000,
		autoplay:true
	});

	$('.header_slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	  	$('.header_slider').attr('data-slick-index',nextSlide).find('.wrap-desc').removeClass('animated bounceInUp slow');
	  	$('.header_slider').attr('data-slick-index',currentSlide).find('.wrap-desc').addClass('animated bounceInUp slow');
	});

	$('.slider_about_us').slick({
		slidesToShow: 2,
		slidesToScroll: 1,
		arrows: true,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
				infinite: true,
			}
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});
	$('.slider_client').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: false,
		autoplaySpeed: 5000,
		autoplay:true,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
			}
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});

	$(document).on('click', 'a[href^="#"]', function(e) {
	    // target element id
	    var id = $(this).attr('href');

	    // target element
	    var $id = $(id);
	    if ($id.length === 0) {
	        return;
	    }

	    // prevent standard hash navigation (avoid blinking in IE)
	    e.preventDefault();

	    // top position relative to the document
	    var pos = $id.offset().top - 75;

	    // animated top scrolling
	    $('body, html').animate({scrollTop: pos});
	});

	$(".js-modal-btn").modalVideo();

});